<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
    public function register()
    {
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
    }

    function enqueue()
    {
        // enqueue all our styles & scripts
        wp_enqueue_style( 'test-plugin-style', $this->plugin_url . '/assets/test_plugin_style.css' );
        wp_enqueue_script( 'test-plugin-script', $this->plugin_url . '/assets/test_plugin_script.js' );
    }
}