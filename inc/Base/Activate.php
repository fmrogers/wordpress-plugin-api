<?php

/**
 * @package ExamplePluginExtended
 */
namespace Inc\Base;

class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();

        if ( get_option( 'example_plugin_extended' ) ) {
        	return;
        }

        $default = [];

        update_option( 'example_plugin_extended', $default );
    }
}