<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;

class CustomTaxonomyController extends BaseController
{
	public $callbacks;

	public $subPages = [];

	public function register()
	{

		if (! $this->activated( 'taxonomy_manager' ) ) return;

		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setSubPages();

		$this->settings
			->addSubPages($this->subpages)
			->register();

		//add_action( 'init', [$this, 'activate'] );
	}

	public function setSubPages()
	{
		$this->subpages = [
			[
                'parent_slug'   => 'example_plugin_extended',
                'page_title'    => 'Taxonomies Manager',
                'menu_title'    => 'CT Manager',
                'capability'    => 'manage_options',
                'menu_slug'     => 'example_custom_taxonomies',
                'callback'      => array( $this->callbacks, 'adminTaxonomy' )
            ],
		];
	}
}