<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;

class CustomPostTypeController extends BaseController
{
	public $callbacks;

	public $subPages = [];

	public function register()
	{
		if (! $this->activated( 'cpt_manager' ) ) return;

		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setSubPages();

		$this->settings
			->addSubPages($this->subpages)
			->register();

		add_action( 'init', [$this, 'activate'] );
	}

	public function setSubPages()
	{
		$this->subpages = [
			[
				'parent_slug'   => 'example_plugin_extended',
				'page_title'    => 'Custom Post Types',
				'menu_title'    => 'CPT Manager',
				'capability'    => 'manage_options',
				'menu_slug'     => 'custom_post_type',
				'callback'      => array( $this->callbacks, 'adminCpt' )
			]
		];
	}

	public function activate()
	{
		register_post_type(
			'example_products',
			[
				'labels' => [
					'name'          => 'Products',
					'singular_term' => 'Product'
				],
				'public'        => true,
				'has_archive'   => true,
			]
		);
	}
}