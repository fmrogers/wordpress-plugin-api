<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;

class TemplatesManager extends BaseController
{
	public $callbacks;

	public $subPages = [];

	public function register()
	{

		if (! $this->activated( 'templates_manager' ) ) return;

		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setSubPages();

		$this->settings
			->addSubPages($this->subpages)
			->register();

		add_action( 'init', [$this, 'activate'] );
	}

	public function setSubPages()
	{
		$this->subpages = [
			[
                'parent_slug'   => 'example_plugin_extended',
                'page_title'    => 'Template Manager',
                'menu_title'    => 'Template Manager',
                'capability'    => 'manage_options',
                'menu_slug'     => 'example_plugin_extended_widgets',
                'callback'      => array( $this->callbacks, 'adminWidget' )
            ]
		];
	}
}