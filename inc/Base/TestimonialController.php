<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;

class TestimonialController extends BaseController
{
	public $callbacks;

	public $subPages = [];

	public function register()
	{

		if (! $this->activated( 'testimonial_manager' ) ) return;

		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setSubPages();

		$this->settings
			->addSubPages($this->subpages)
			->register();

		add_action( 'init', [$this, 'activate'] );
	}

	public function setSubPages()
	{
		$this->subpages = [
			[
                'parent_slug'   => 'example_plugin_extended',
                'page_title'    => 'Testimonial Manager',
                'menu_title'    => 'Testimonial Manager',
                'capability'    => 'manage_options',
                'menu_slug'     => 'example_plugin_extended_widgets',
                'callback'      => array( $this->callbacks, 'adminWidget' )
            ]
		];
	}
}