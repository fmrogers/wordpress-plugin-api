<?php

/**
 * @package ExamplePluginExtended
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
    public function adminDashboard()
    {
        return require_once( "$this->plugin_path/templates/admin.php");
    }

    public function adminCpt()
    {
        return require_once( "$this->plugin_path/templates/cpt.php" );
    }

    public function adminTaxonomy()
    {
        return require_once( "$this->plugin_path/templates/taxonomy.php" );
    }

    public function adminWidget()
    {
        return require_once( "$this->plugin_path/templates/widget.php" );
    }

	public function firstNameExample()
	{
		$value = esc_attr( get_option( 'first_name' ) );
		echo '<input type="text" class="regular-text" name="text_example" value="' . $value . '" placeholder="Write your first name!">';
	}

	public function lastNameExample()
	{
		$value = esc_attr( get_option( 'last_name' ) );
		echo '<input type="text" class="regular-text" name="text_example" value="' . $value . '" placeholder="Write your last name!">';
	}
}