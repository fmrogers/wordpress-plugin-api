<?php

/**
 * @package TestPluginExtended
 */

namespace Inc\Pages;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;
use \Inc\Api\Callbacks\ManagerCallbacks;

class Dashboard extends BaseController
{
    public $settings;

    public $callbacks;
	public $callbacks_mngr;

    public $pages = [];
    //public $subpages = [];

    public function register()
    {
        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();
        $this->callbacks_mngr = new ManagerCallbacks();

        $this->setPages();
        //$this->setSubPages();

        $this->setSettings();
        $this->setSections();
        $this->setFields();

        $this->settings
            ->addPages( $this->pages )
            ->withSubPage( 'Dashboard' )
            //->addSubPages( $this->subpages )
            ->register();
    }

    public function setPages()
    {
        $this->pages = [
            [
                'page_title'    => 'Example Settings',
                'menu_title'    => 'Settings',
                'capability'    => 'manage_options',
                'menu_slug'     => 'example_plugin_extended',
                'callback'      => array( $this->callbacks, 'adminDashboard' ),
                'icon_url'      => 'dashicons-admin-generic',
                'position'      => 110
            ]
        ];
    }

//    public function setSubPages()
//    {
//        $this->subpages = [
//            [
//                'parent_slug'   => 'example_plugin_extended',
//                'page_title'    => 'Custom Post Types',
//                'menu_title'    => 'CPT',
//                'capability'    => 'manage_options',
//                'menu_slug'     => 'example_plugin_extended_cpt',
//                'callback'      => array( $this->callbacks, 'adminCpt' )
//            ],
//            [
//                'parent_slug'   => 'example_plugin_extended',
//                'page_title'    => 'Custom Taxonomies',
//                'menu_title'    => 'Taxonomies',
//                'capability'    => 'manage_options',
//                'menu_slug'     => 'example_plugin_extended_taxonomies',
//                'callback'      => array( $this->callbacks, 'adminTaxonomy' )
//            ],
//            [
//                'parent_slug'   => 'example_plugin_extended',
//                'page_title'    => 'Custom Widgets',
//                'menu_title'    => 'Widgets',
//                'capability'    => 'manage_options',
//                'menu_slug'     => 'example_plugin_extended_widgets',
//                'callback'      => array( $this->callbacks, 'adminWidget' )
//            ]
//        ];
//    }

    public function setSettings()
    {
        $args = [
        	[
		        'option_group'    => 'example_plugin_settings',
		        'option_name'     => 'example_plugin_extended',
		        'callback'        => [ $this->callbacks_mngr, 'checkboxSanitize' ]
	        ]
        ];

        $this->settings->setSettings( $args );
    }

	public function setSections()
	{
		$args = [
			[
				'id'          => 'example_admin_index',
				'title'       => 'Settings Manager',
				'callback'    => [ $this->callbacks_mngr, 'adminSectionManager' ],
				'page'        => 'example_plugin_extended'
			]
		];

		$this->settings->setSections( $args );
	}

	public function setFields()
	{
		$args = [];

		foreach ( $this->managers as $id => $title ) {
			$args[] = [
				'id'        => $id,
				'title'     => $title,
				'callback'  => [ $this->callbacks_mngr, 'checkboxField' ],
				'page'      => 'example_plugin_extended',
				'section'   => 'example_admin_index',
				'args'      => [
					'option_name'   => 'example_plugin_extended',
					'label_for'     => $id,
					'class'         => 'ui-toggle'
				]
			];
		}

		$this->settings->setFields( $args );
	}
}