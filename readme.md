# Project Overview

* Modular Administration Area
* Custom Post Type (CPT) Manager
* Custom Taxonomy Manager
* Widget to Upload and display media in sidebars
* Post and Pages Gallery Integration
* Testimonial section: Comment in the front-end, Admin can approve comments, select which comment to display
* Custom template section
* Ajax based Login/Register system
* Membership protected area
* Chat System