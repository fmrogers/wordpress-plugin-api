<?php


/**
 * Trigger this file on plugin uninstall
 *
 * @package TestPlugin
 */


if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    die;
}

// Access the database via SQL
global $wpdb;
$wpdb->query( "DELETE FROM {$wpdb->prefix}posts WHERE post_type = 'acme_product'" );
$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE post_id NOT IN (SELECT id FROM {$wpdb->prefix}posts)" );
$wpdb->query( "DELETE FROM {$wpdb->prefix}term_relationships WHERE object_id NOT IN (SELECT id FROM {$wpdb->prefix}posts)" );