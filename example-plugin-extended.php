<?php

/**
 * @package ExamplePluginExtended
 */

/*
Plugin Name: Example Plugin Extended
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: This is a simple boilerplate example of a WordPress plugin.
Version: 1.0
Author: Frederick M. Rogers
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

/**
 * If this file called directly, abort!
 */
defined( 'ABSPATH' ) or die( 'Looks like you made a wrong turn there buddy' );

/**
 * Require once the Composer Autoload
 */
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
    require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * WordPress requires both the Activate and Deactivate
 * methods be run from a procedural function as the hook
 * will not allow for a static method call from a class.
 */

/**
 * Procedural function that runs during plugin activation.
 */
function activate_example_plugin_extended() {
    Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_example_plugin_extended' );

/**
 * Procedural function that runs during plugin deactivation.
 */
function deactivate_example_plugin_extended() {
    Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_example_plugin_extended' );

/**
 * Initialize all the core classes of the plugin
 */
if ( class_exists( 'Inc\\Init' ) ) {
    Inc\Init::register_services();
}